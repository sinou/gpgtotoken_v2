package edu.ufl.cise;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.StringReader;
//import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.dcoref.CorefChain;
import edu.stanford.nlp.dcoref.CorefCoreAnnotations.CorefChainAnnotation;
import edu.stanford.nlp.ie.AbstractSequenceClassifier;
import edu.stanford.nlp.ie.crf.CRFClassifier;
import edu.stanford.nlp.process.TokenizerFactory;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreAnnotations.NamedEntityTagAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.Sentence;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations.CollapsedCCProcessedDependenciesAnnotation;
import edu.stanford.nlp.trees.*;
import edu.stanford.nlp.trees.TreeCoreAnnotations.TreeAnnotation;
import edu.stanford.nlp.util.CoreMap;
import edu.stanford.nlp.util.PropertiesUtils;
import edu.stanford.nlp.util.StringUtils;
import edu.stanford.nlp.util.TypesafeMap.Key;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.parser.lexparser.ParserAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
//import edu.stanford.nlp.pipeline.TokensRegexNERAnnotator;
import edu.stanford.nlp.ie.crf.*;
import edu.stanford.nlp.ie.AbstractSequenceClassifier;
import edu.stanford.nlp.io.IOUtils;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.CoreAnnotations.AnswerAnnotation;
import edu.stanford.nlp.ling.tokensregex.SequencePattern.PatternExpr;
import edu.stanford.nlp.ling.tokensregex.TokenSequenceMatcher;
import edu.stanford.nlp.ling.tokensregex.TokenSequencePattern;
import edu.stanford.nlp.util.StringUtils;

import java.util.List;
import java.io.IOException;

import java.io.File;
import java.io.FileWriter;

import com.sleepycat.je.DatabaseException;


class ParserDemo {

  /**
   * The main method demonstrates the easiest way to load a parser.
   * Simply call loadModel and specify the path, which can either be a
   * file or any resource in the classpath.  For example, this
   * demonstrates loading from the models jar file, which you need to
   * include in the classpath for ParserDemo to work.
 * @throws IOException 
   */

  HashMap map = new HashMap();
  int count = 0;
  
  public static void main(String[] args) throws IOException {
    ParserDemo pd = new ParserDemo();
    LexicalizedParser lp = LexicalizedParser.loadModel("/home/srini/Downloads/englishPCFG.ser.gz");
    long temp;
    System.err.println("Free memory (bytes): " + 
            Runtime.getRuntime().freeMemory());
    System.err.println("Max memory (bytes): " + Runtime.getRuntime().maxMemory());
    if (args.length > 0) {
        
    	switch(args[1]){
        
    	case "TOK":
        temp = System.currentTimeMillis();
	System.err.println("here");
        pd.demoTOK(args[0]);
        System.err.println("Time taken: " + (System.currentTimeMillis() - temp) + " millis");
        break;
    	case "SENT":
        temp = System.currentTimeMillis();
        pd.demoSENT(args[0], 0, "");
        System.err.println("Time taken: " + (System.currentTimeMillis() - temp) + " millis");
        break;
    	case "POS":
        temp = System.currentTimeMillis();
        pd.demoPOS(args[0]);
        System.err.println("Time taken: " + (System.currentTimeMillis() - temp) + " millis");
        break;
    	case "NER":
        temp = System.currentTimeMillis();
        pd.demoNER(args[0]);
        System.err.println("Time taken: " + (System.currentTimeMillis() - temp) + " millis");
        break;
    	case "NP":
        temp = System.currentTimeMillis();
        pd.demoNP(args[0]);
        System.err.println("Time taken: " + (System.currentTimeMillis() - temp) + " millis");
        break;
    	//case "REGEX":
        //temp = System.currentTimeMillis();
        //demoREGEX(args[0]);
        //System.err.println("Time taken: " + (System.currentTimeMillis() - temp) + " millis");
        //break;
    	}
        
    } 
  }

  /**
   * demoDP demonstrates turning a file into tokens and then parse
   * trees.  Note that the trees are printed by calling pennPrint on
   * the Tree object.  It is also possible to pass a PrintWriter to
   * pennPrint if you want to capture the output.
 * @throws FileNotFoundException 
   */
 
  
  public void test(){
   
  System.out.println("---printed from ParserDemo.java---");

  }

  public void writeToFile(String pipeline, String content){
  
        FileWriter fileWriter = null;
	try{
    		File newTextFile = new File("/home/srini/Documents/gatordsr/stream/" + pipeline + ".txt");
                fileWriter = new FileWriter(newTextFile);
            	fileWriter.write(content);
            	fileWriter.close();
	}catch(Exception e){
		System.err.println("Error during writing file - ParserDemo.Java");
	}
  
  }
  
  public void demoTOK(String filename) throws IOException {
	    ParserDemo pd = new ParserDemo();
	    String content = "";
	    Properties props = new Properties();
	    props.put("annotators", "tokenize");
	    StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
	    String text = IOUtils.slurpFile(filename);
	    Annotation document = new Annotation(text);
	    pipeline.annotate(document);
	    
	    for( CoreLabel token:  document.get(TokensAnnotation.class)){
	    	content = content + '\n' + token;
	    }
	    pd.writeToFile("TOK", content);
	    
	    
	    
  }
  
  
 public void demoSENT(String filename, int wordCount, String dbName) throws IOException {
	 map = new HashMap();
	 count = 0;
	 SimpleStorePut oSimpleStorePut = new SimpleStorePut(dbName);
	    ParserDemo pd = new ParserDemo();
	    String content = "";
	  Properties props = new Properties();
	    props.put("annotators", "tokenize, ssplit");
	    StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
	    String text = filename;
	    Annotation document = new Annotation(text);
	    pipeline.annotate(document);
	    int[] arrayOfTokens = new int[wordCount];
	    int localCount = 0;
	    List<CoreMap> sentences = document.get(SentencesAnnotation.class);
	    
	    for(CoreMap sentence: sentences) {
	        // traversing the words in the current sentence
	        // a CoreLabel is a CoreMap with additional token-specific methods
		//System.out.println(sentence);
		content = content + '\n' + sentence;
		arrayOfTokens[count] = sentence.get(TokensAnnotation.class).size();
		count++;
	        for (CoreLabel token: sentence.get(TokensAnnotation.class)) {
	          String word = token.get(TextAnnotation.class);
		  
		  
		  if(map.get(word) == null){
			  oSimpleStorePut.triggerPut(new String[]{ ""+map.size(), word});
			  localCount++;
			  map.put(word, new Integer(map.size()));
		  }else{
			  int x = 0;
		  }
		  arrayOfTokens[count] = (int)map.get(word);
		  count++;
	          //System.out.println(word);
	        }
	     	        
	    }
	    int breakHere = arrayOfTokens[0];
	    int flag = 1;
	    String value = "";
	    LinkedList<String> temp = new LinkedList<String>();
	    for(int i = 0; i < count; i++){
	    	if(flag == 1){
	    		flag = 0;
//	    		System.out.print(arrayOfTokens[i] + " - ");
	    	}else{
//	    		System.out.print(arrayOfTokens[i] + ", ");
	    		value += arrayOfTokens[i] + ", ";
	    	}
			
			breakHere--;
			if(breakHere < 0){
				temp.add(value);
				value = "";
				breakHere = arrayOfTokens[i + 1];
				System.out.println();
				flag = 1;
			}
			
			
			}
	    String[] tokenKeysArray = new String[temp.size()];
		int i = 0;
		for(String s: temp){
			//System.out.println(s);
			tokenKeysArray[i++] = s;
		}
	    System.out.println();
	    System.out.println();
	    System.out.println();
	    
	    JavaSequenceFile.createSequenceFile(tokenKeysArray);
	    
	    try {
			oSimpleStorePut.end();
		} catch (DatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    SimpleStoreGet.triggerGet(localCount, dbName);
            	pd.writeToFile("SENT", content);
		
		
	    
  }


  
 public void demoPOS(String filename) throws IOException {
	  
	    ParserDemo pd = new ParserDemo();
	    String content = "";
	    Properties props = new Properties();
	    props.put("annotators", "tokenize, ssplit, pos");
	    StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
	    String text = IOUtils.slurpFile(filename);
	    Annotation document = new Annotation(text);
	    pipeline.annotate(document);
	    
	  
	    List<CoreMap> sentences = document.get(SentencesAnnotation.class);
	    
	    for(CoreMap sentence: sentences) {
	        for (CoreLabel token: sentence.get(TokensAnnotation.class)) {
	          String word = token.get(TextAnnotation.class);
	          
	          String pos = token.get(PartOfSpeechAnnotation.class);
	          content = content + '\n' + word + " - " + pos;
	          //System.out.println(word + "--" + pos);
	        }
	        
		      
		      	     
	        
	    }
	    pd.writeToFile("POS", content);

	    
 }
 
  
  
  public void demoNER(String filename) throws IOException {
	    ParserDemo pd = new ParserDemo();
	    String content = "";
	    Properties props = new Properties();
	    props.put("annotators", "tokenize, ssplit, pos, lemma, ner, parse, dcoref");
	    StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
	    String text = IOUtils.slurpFile(filename);
	    Annotation document = new Annotation(text);
	    pipeline.annotate(document);
	    
	  
	    List<CoreMap> sentences = document.get(SentencesAnnotation.class);
	    int i = 0;
	    for(CoreMap sentence: sentences) {
		if(i < 2){
	        // traversing the words in the current sentence
	        // a CoreLabel is a CoreMap with additional token-specific methods
	        for (CoreLabel token: sentence.get(TokensAnnotation.class)) {
	          // this is the text of the token
	          String word = token.get(TextAnnotation.class);
	          // this is the POS tag of the token
	          String pos = token.get(PartOfSpeechAnnotation.class);
	          // this is the NER label of the token
	          String ne = token.get(NamedEntityTagAnnotation.class); 
		  content = content + word + " --- " + ne + '\n'+ '\n'+ '\n'+ '\n';      
	        }
	        
	        Tree tree = sentence.get(TreeAnnotation.class);
		      //System.out.println(tree);
		      content = content + '\n'+ '\n'+ '\n'+ '\n' + tree;
		      SemanticGraph dependencies = sentence.get(CollapsedCCProcessedDependenciesAnnotation.class);
		      
		      	     
	        i++;
		}
	    }
	    
	    Map<Integer, CorefChain> graph = 
	    	      document.get(CorefChainAnnotation.class);
	    	    //System.out.println(graph);
		    content = content + '\n'+ '\n'+ '\n'+ '\n' + graph;

	    pd.writeToFile("NER", content);
  }
  
//  
public void demoNP(String filename) throws IOException {

	  ParserDemo pd = new ParserDemo();
	  String content = "";
	  
	  Properties props = new Properties();
	    props.put("annotators", "tokenize, ssplit, parse");
	    StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
	    String text = IOUtils.slurpFile(filename);
	    Annotation document = new Annotation(text);
	    pipeline.annotate(document);
	    
	  
	    List<CoreMap> sentences = document.get(SentencesAnnotation.class);
	    
	            
	    
  }
  

/*public static void demoREGEX(String filename) throws IOException {
	  
	  Properties props = new Properties();
	    props.put("annotators", "tokenize, ssplit, pos, lemma, ner, parse, dcoref, regexner");
	    StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
	    String text = IOUtils.slurpFile(filename);
	    Annotation document = new Annotation(text);
	    pipeline.annotate(document);
	    
	  
	    List<CoreMap> sentences = document.get(SentencesAnnotation.class);
	    
	    for(CoreMap sentence: sentences) {
	        // traversing the words in the current sentence
	        // a CoreLabel is a CoreMap with additional token-specific methods
	        for (CoreLabel token: sentence.get(TokensAnnotation.class)) {
	          // this is the text of the token
	          String word = token.get(TextAnnotation.class);
	          // this is the POS tag of the token
	          String pos = token.get(PartOfSpeechAnnotation.class);
	          // this is the NER label of the token
	          String ne = token.get(NamedEntityTagAnnotation.class);
//	          This is regexlabel
//	          String regex = token.get((Class<? extends Key<VALUE>>) );
	        }
	        
	        //List<CoreLabel> tokens = sentence.get(TokensAnnotation.class);
	        //TokenSequencePattern pattern = TokenSequencePattern.compile((PatternExpr) sentence.get(TokensAnnotation.class));
	        //TokenSequenceMatcher matcher = pattern.getMatcher(tokens);
	        
	        //while (matcher.find()) {
	            //String matchedString = matcher.group();
	            //List<CoreLabel> matchedTokens = (List<CoreLabel>) matcher.groupNodes();
	            //System.out.println(matchedTokens);
	          }
	        
	        
	    }
	    
	    Map<Integer, CorefChain> graph = 
	    	      document.get(CorefChainAnnotation.class);
	    	    System.out.println(graph);

	    
}*/
  
 



}
