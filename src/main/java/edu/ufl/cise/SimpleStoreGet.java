package edu.ufl.cise;

import java.io.File;

import com.sleepycat.je.DatabaseException; 
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;

import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.StoreConfig;

import java.io.FileNotFoundException;

import edu.ufl.cise.SimpleEntityClass;

public class SimpleStoreGet {

    private static File envHome = null;

    private Environment envmnt;
    private EntityStore store;
    private SimpleDA sda; 
    
    public SimpleStoreGet(String fileName) {
    	super();
    	envHome = new File("./"+fileName);
    }

   // The setup() method opens the environment and store
    // for us.
    public void setup()
        throws DatabaseException, FileNotFoundException{

        try {
            EnvironmentConfig envConfig = new EnvironmentConfig();
            StoreConfig storeConfig = new StoreConfig();

            // Open the environment and entity store
            envmnt = new Environment(envHome, envConfig);
            store = new EntityStore(envmnt, "EntityStore", storeConfig);
        }catch (Exception fnfe) {
            System.err.println("setup(): " + fnfe.toString());
            System.exit(-1);
        }
    } 

    public void shutdown()
        throws DatabaseException {

        store.close();
        envmnt.close();
    } 


    private void run(int size)
        throws DatabaseException, FileNotFoundException{

        setup();

        // Open the data accessor. This is used to store
        // persistent objects.
        sda = new SimpleDA(store);

        // Instantiate and store some entity classes
        SimpleEntityClass sec;
        for(int i = 0; i < size; i++){
        	sec = sda.pIdx.get(i + "");
        	System.out.println(sec.getpKey() + "   -   " + sec.getsKey());
        }

        



        sda.close();
        shutdown();
    } 

    public static void triggerGet(int size, String dbName) {
        SimpleStoreGet ssg = new SimpleStoreGet(dbName);
        try {
            ssg.run(size);
        } catch (DatabaseException dbe) {
            System.err.println("SimpleStoreGet: " + dbe.toString());
            dbe.printStackTrace();
        } catch (Exception e) {
            System.out.println("Exception: " + e.toString());
            e.printStackTrace();
        } 
        System.out.println("All done.");
    } 

}